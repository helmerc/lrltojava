import java.io.FileNotFoundException;
import java.util.*;


/**
 * @author Christopher Helmer
 * @author Loren Milliman
 *
 */
public class LRLTreeTester {

	public static void main(String[] args) throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter file name: ");
		String fileName = scan.next();
		LRLTree tr = new LRLTree(fileName);
		tr.print();
		System.out.println();
		tr.eval();
		System.out.println(tr.toJavaString());
	}
}
