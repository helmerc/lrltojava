
/**
 * @author Christopher Helmer
 * @author Loren Milliman
 *
 */
public class TreeNode {

	String data;
	TreeNode left;
	TreeNode right;
	
	
	public TreeNode() {
		data = null;
		right = null;
		left = null;
	}
	
	public TreeNode(String aData) {
		data = aData;
		right = null;
		left = null;
	}
}
