# README #

* Simple LRL to Java translator

### How do I get set up? ###

* Run LRLTreeTester.java
* Use LRLFib.txt or any other .txt file containing LRL code to translate.

## Authors ##

* Christopher Helmer
* Loren Milliman

### Who do I talk to? ###

* Christopher Helmer
* christopher.helmer@yahoo.com